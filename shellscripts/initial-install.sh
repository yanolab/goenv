#!/usr/bin/env bash
#
# Environment variable GOENVGOROOT, GOENVTARGET is install target of `goenv`
# Default install target i /usr/local/bin.

if [ -z "$GOENVGOROOT" ]; then
  echo "[goenv] set GOENVGOROOT environment variable. (Go binary install target)"
  exit 0
fi

if [ -z "$GOENVTARGET" ]; then
  echo "[goenv] set GOENVTARGET environment variable. (goenv binary install target)"
  exit 0
fi

cd /tmp

downloader=""
if type curl &> /dev/null; then
  downloader="curl -O"
elif type wget &> /dev/null; then
  downloader="wget"
else
  echo "[goenv] `curl` or `wget` is required to fetch files."
  exit 0
fi

if $downloader "https://bitbucket.org/ymotongpoo/goenv/raw/master/shellscripts/goenvwrapper.sh"; then
  if [ -f "goenvwrapper.sh" ]; then
    source goenvwrapper.sh
  else
    echo "[goenv] cannot read goenvwrapper.sh"
    exit 0
  fi
else
  echo "[goenv] cannot download goenvwrapper.sh"
  exit 0
fi

if goof goinstall default; then
  if [ -d "$GOENVGOROOT/default" ]; then
    echo "[goenv] add following lines in your .barshrc/.zshrc"
    echo "wxport GOROOT=$GOENVGOROOT/default"
    echo "export PATH=$PATH:$GOROOT/bin"
    export GOROOT="$GOENVGOROOT/default"
    export PATH="$GOENVGOROOT/default/bin:$PATH"
  fi
else
  echo "[goenv] `goof goinstall` command does not work."
  exit 0
fi

if ! type git 2>&1 > /dev/null; then
  echo "[goenv] To get goenv, install git"
  exit 0 
fi

echo "[goenv] cloning goenv from GitHub"
if git clone https://bitbucket.org/ymotongpoo/goenv.git; then
  cd goenv
  if ! go build -o goenv; then
    echo "[goenv] cannot build goenv"
  fi
  if ! cp goenv "$GOENVTARGET"; then
    echo "[goenv] cannot copy goenv. check permission of '$GOENVTARGET'"
  fi
  if ! cp shellscripts/goenvwrapper.sh $GOENVTARGET; then
    echo "[goenv] failed copying goenvwrapper.sh. check permission of '$GOENVTARGET'"
  fi
else
  echo "[goenv] cannot clone goenv repository"
  exit 0
fi

echo "[goenv] install finished."
exit 0